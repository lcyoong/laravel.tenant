let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.options({ processCssUrls: false });

mix.js('resources/assets/js/app.js', 'public/js')
  //  .js('resources/assets/theme/assets/global/scripts/app.js', 'public/assets/global/scripts')
  //  .js('resources/assets/theme/assets/layouts/layout3/scripts/layout.js', 'public/assets/layouts/layout3/scripts')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .sass('resources/assets/sass/layouts/layout3/layout.scss', 'public/assets/layouts/layout3/css')
   .sass('resources/assets/sass/layouts/layout3/custom.scss', 'public/assets/layouts/layout3/css')
   .sass('resources/assets/sass/layouts/layout3/themes/default.scss', 'public/assets/layouts/layout3/css/themes')
   .sass('resources/assets/sass/pages/profile.scss', 'public/assets/pages/css')
   .sass('resources/assets/sass/global/plugins.scss', 'public/assets/global/css')
   .sass('resources/assets/sass/global/components.scss', 'public/assets/global/css');

mix.copy(
   'node_modules/toastr/build',
   'public/build/toastr'
);

mix.copy(
   'node_modules/jquery-ui-dist/jquery-ui.min.js',
   'public/js/jquery-ui.min.js'
);

mix.copy(
   'resources/assets/theme/assets/layouts/layout3/scripts/layout.js',
   'public/assets/layouts/layout3/scripts/layout.js'
);

mix.copy(
   'resources/assets/theme/assets/global/scripts/app.js',
   'public/assets/global/scripts/app.js'
);

mix.copy(
   'resources/assets/theme/assets/global/img',
   'public/assets/global/img'
);

mix.copy(
   'resources/assets/theme/assets/global/plugins/simple-line-icons',
   'public/assets/global/plugins/simple-line-icons'
);

mix.copy(
   'resources/assets/theme/assets/global/plugins/font-awesome/fonts',
   'public/fonts'
);
