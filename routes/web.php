<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('logout', 'Auth\LoginController@logout');

Auth::routes();

Route::get('/landing', 'ConsoleDashboardController@landing');

Route::post('change_password', 'Auth\ChangePasswordController@update');

Route::group(['prefix' => 'console', 'middleware'=> ['auth']], function () {

  Route::get('/', 'ConsoleDashboardController@select');

  Route::get('change_password', 'Auth\ChangePasswordController@console');

  Route::group(['prefix' => '{tenant}', 'middleware'=> ['assigned']], function () {

    Route::get('/', 'ConsoleDashboardController@index');

    Route::group(['prefix' => 'prospects'], function () {
      Route::get('/', 'ProspectController@index');
    });

  });

});


Route::group(['prefix' => 'admin', 'middleware'=> ['auth', 'role:super_admin|wgops']], function () {

  Route::get('/', 'DashboardController@index');

  Route::get('change_password', 'Auth\ChangePasswordController@admin');
  // Route::post('change_password', 'Auth\ChangePasswordController@update');

  Route::group(['middleware' => ['permitted:manage_tenant'], 'prefix' => 'tenants'], function () {
    Route::get('/', 'TenantController@index');
    Route::get('/new', 'TenantController@create');
    Route::get('/{tenant}/edit', 'TenantController@edit');
    Route::get('/{tenant}/users', 'TenantUserController@index');

    Route::post('/new', 'TenantController@store');
    Route::post('/update', 'TenantController@update');
    Route::post('/{tenant}/connect', 'TenantController@connect');
    Route::post('/{tenant}/users/new', 'TenantUserController@attach');
    Route::post('/{tenant}/users/{user}/remove', 'TenantUserController@detach');
  });

  Route::group(['middleware' => ['permitted:manage_user'], 'prefix' => 'users'], function () {
    Route::get('/', 'UserController@index');
    Route::get('/new', 'UserController@create');
    Route::get('/{user}/edit', 'UserController@edit');
    Route::post('/new', 'UserController@store');
    Route::post('/update', 'UserController@update');
  });

  Route::group(['middleware' => ['permitted:manage_role'], 'prefix' => 'roles'], function () {
    Route::get('/', 'RoleController@index');
    Route::get('/new', 'RoleController@create');
    Route::get('/{role}/edit', 'RoleController@edit');
    Route::get('/{role_id}/permission', 'RoleController@permission');
    Route::post('/new', 'RoleController@store');
    Route::post('/update', 'RoleController@update');
    Route::post('/{role}/permissions', 'RoleController@syncPermission');
  });

  Route::group(['middleware' => ['permitted:manage_permission'], 'prefix' => 'permissions'], function () {
    Route::get('/', 'PermissionController@index');
    Route::get('/new', 'PermissionController@create');
    Route::get('/{role}/edit', 'PermissionController@edit');
    Route::post('/new', 'PermissionController@store');
    Route::post('/update', 'PermissionController@update');
  });

  Route::group(['prefix' => '/settings', 'middleware' => ['permitted:manage_settings']], function () {
    Route::get('/', 'SettingController@index');
    Route::get('/new', 'SettingController@create');
    Route::get('/{setting}/edit', 'SettingController@edit');
    Route::post('/new', 'SettingController@store');
    Route::post('/update', 'SettingController@update');
  });
});
