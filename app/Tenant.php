<?php

namespace App;

class Tenant extends BaseModel
{
  protected $audit = true;

  protected $primaryKey = 'ten_id';

  protected $fillable = ['ten_name', 'ten_expiry', 'ten_status', 'ten_db', 'created_by'];

  public function users()
  {
    return $this->belongsToMany(User::class, 'tenant_users', 'tenu_tenant', 'tenu_user')->withTimestamps();
  }
}
