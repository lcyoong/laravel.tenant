<?php

namespace Filters;

class SettingFilter extends QueryFilter
{
  /**
   * Name filter
   * @param  string $value
   * @return Builder
   */
  public function code($value = '')
  {
    return $this->builder->where('set_code', 'like', "%$value%");
  }
  
}
