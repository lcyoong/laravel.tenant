<?php

namespace Filters;

class PageFilter extends QueryFilter
{
  /**
   * Name filter
   * @param  string $value
   * @return Builder
   */
  public function name($value = '')
  {
    return $this->builder->where('pg_name', 'like', "%$value%");
  }

  /**
   * Status filter
   * @param  string $value
   * @return Builder
   */
  public function status($value = 'active')
  {
    return $this->builder->where("pg_status", '=', $value);
  }

}
