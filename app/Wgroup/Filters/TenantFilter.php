<?php

namespace Filters;

class TenantFilter extends QueryFilter
{
  /**
   * Name filter
   * @param  string $value
   * @return Builder
   */
  public function name($value = '')
  {
    return $this->builder->where('acc_name', 'like', "%$value%");
  }

  /**
   * Status filter
   * @param  string $value
   * @return Builder
   */
  public function status($value = 'active')
  {
    return $this->builder->where("acc_status", '=', $value);
  }

}
