<?php

namespace Filters;

class UserFilter extends QueryFilter
{
  /**
   * Name filter
   * @param  string $value
   * @return Builder
   */
  public function id($value = '')
  {
    return $this->builder->where('id', '=', $value);
  }

  /**
   * Name filter
   * @param  string $value
   * @return Builder
   */
  public function name($value = '')
  {
    return $this->builder->where('name', 'like', "%$value%");
  }

  /**
   * Email filter
   * @param  string $value
   * @return Builder
   */
  public function email($value = '')
  {
    return $this->builder->where('email', 'like', "%$value%");
  }

  /**
   * Status filter
   * @param  string $value
   * @return Builder
   */
  public function status($value = 'active')
  {
    return $this->builder->where("bil_status", '=', $value);
  }

}
