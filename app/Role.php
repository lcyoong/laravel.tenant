<?php

namespace App;

use Zizaco\Entrust\EntrustRole;
use App\Traits\ModelTrait;

class Role extends EntrustRole
{
  use ModelTrait;

  protected $fillable = ['name', 'display_name'];
}
