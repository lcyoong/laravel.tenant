<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prospect extends TenantModel
{
  protected $appends = ['pro_full_name'];

  public function getProFullNameAttribute()
  {
    return trim($this->pro_first_name . ' ' . $this->pro_last_name);
  }
}
