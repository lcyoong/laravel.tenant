<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Notifications\ResetPassword;
use App\Traits\ModelTrait;

class User extends Authenticatable
{
  use Notifiable, HasApiTokens;
  use EntrustUserTrait;
  use ModelTrait;

  protected $table = 'users';
  protected $primary_key = 'id';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'name', 'email', 'password',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
      'password', 'remember_token',
  ];

  public function sendPasswordResetNotification($token)
  {
    $this->notify(new ResetPassword($this, $token));
  }

  public function owns($related, $field)
  {
      return $this->id == $related->$field;
  }

  public function tenants()
  {
    return $this->belongsToMany(Tenant::class, 'tenant_users', 'tenu_user', 'tenu_tenant')->withTimestamps();
  }

}
