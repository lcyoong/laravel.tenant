<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\AuditTrailRelationship;
use App\Traits\ModelTrait;
use App\Events\UserNeeded;
use App\Events\ModelCreated;
use App\Events\ModelUpdated;

class BaseModel extends Model
{
  use AuditTrailRelationship;
  use ModelTrait;

  protected $audit = false;

  public static function boot()
  {
    parent::boot();

    Self::creating(function ($post) {

      if (in_array('created_by', $post->fillable)) {
        if (!empty(auth()->user())) {
          $post->created_by = auth()->user()->id;
        } else {
          $post->created_by = 0;
        }
      }

    });

    Self::updated(function ($post) {

      event(new ModelUpdated($post));

    });

    Self::created(function ($post) {

      event(new ModelCreated($post));

      event(new UserNeeded($post));

    });

  }
}
