<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
      'App\Events\TenantConnected' => [
        'App\Listeners\CreateAndMigrateDatabase',
      ],

      'App\Events\UserNeeded' => [
        'App\Listeners\CreateUser',
      ],

      'App\Events\ModelCreated' => [
        'App\Listeners\AuditLogCreated',
      ],

      'App\Events\ModelUpdated' => [
        'App\Listeners\AuditLogUpdated',
      ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
