<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Eloquent\Relations\Relation;
use Form;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      Validator::extend( 'unique_but_self', function ( $attribute, $value, $parameters, $validator ) {

          // remove first parameter and assume it is the table name
          $table = array_shift( $parameters );

          $unique_field = array_shift( $parameters );

          $unique_value = !is_null($unique_field) ? array_get($validator->getData(), $unique_field, '') : '';

          $self_field = array_shift( $parameters );

          $self_id = !is_null($self_field) ? array_get($validator->getData(), $self_field, 0) : 0;

          // query the table with all the conditions
          $result = \DB::table( $table )->select( \DB::raw( 1 ) )->where($unique_field, '=', $unique_value)->where($self_field, '!=', $self_id)->first();

          return empty( $result ); // edited here
      });

      Blade::directive('permitted', function ($expression) {

        return "<?php if (Auth::user()->can($expression) || Auth::user()->hasRole('super_admin')): ?>";

      });

      Blade::directive('endpermitted', function ($expression) {

        return "<?php endif; ?>";

      });

      Blade::directive('url', function ($expression) {

        $value = session('tenant')->$expression;

        return url("/console/" . session('tenant')->ten_id . "/" . $expression);

      });

      Blade::directive('activeAdminMenu', function ($arguments) {

        list($expression1, $expression2) = explode(',',str_replace(['(',')',' ', "'"], '', $arguments));

        return "<?php if($expression1 == $expression2) echo 'active'; ?>";

      });

      Form::macro('linkButton', function ($url, $text) {
        return '<a href="'.$url.'">' . Form::button($text, ['class' => 'btn btn-primary btn-sm btn-circle red']) . '</a>';
      });

      Form::macro('labelWithHTML', function ($name, $html) {
          return '<label for="'.$name.'">'.$html.'</label>';
      });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
