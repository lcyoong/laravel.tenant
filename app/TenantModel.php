<?php

namespace App;

use Wgroup\Tenant;

class TenantModel extends BaseModel
{
  /**
   * Create a new model instance.
   * @param array $attributes
   */
  public function __construct($attributes = [])
	{
    parent::__construct($attributes);

    $tenant = session('tenant');

    $database = "owlsight_" . $tenant->ten_db;

    Tenant::connect(['database' => $database]);

    $this->connection = $database;
	}
}
