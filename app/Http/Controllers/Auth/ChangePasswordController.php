<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\BaseController;
use App\Http\Requests\ChangePassword;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class ChangePasswordController extends BaseController
{
  /**
   * Create a new controller instance.
   * @param AssignmentRepository $repo
   */
  public function __construct()
  {
    parent::__construct();
  }

  public function admin(Request $request)
  {
    $this->page_title = trans('user.change_password');

    return view('auth.passwords.change', $this->vdata);
  }

  public function update(ChangePassword $request)
  {
    $user = auth()->user();

    $user->update(['password' => bcrypt($request->password)]);

    return $this->goodReponse();
  }
}
