<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends AdminController
{
    public function __construct()
    {
      parent::__construct();

      $active = 'dashboard';

      $this->vdata(compact('active'));
    }

    public function index()
    {
      return view('dashboard.admin', $this->vdata);
    }
}
