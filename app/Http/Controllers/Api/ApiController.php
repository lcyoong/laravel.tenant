<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests;

class ApiController extends Controller
{
  public function goodReponse($message = '', $data = null, $cookie = null)
  {
    return response([
      'success' => true,
      'data' => $data,
      'message' => $message ? $message : trans('message.process_successful')
    ])->withCookie(cookie('lead', 'hello@vv.com', 3600));

    return $response;
  }

}
