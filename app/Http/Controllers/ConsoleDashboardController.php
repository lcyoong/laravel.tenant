<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;

class ConsoleDashboardController extends ConsoleController
{
  public function __construct()
  {
    parent::__construct();

    $active = 'dashboard';

    $this->vdata(compact('active'));
  }

  public function landing()
  {
    $user = Auth()->user();

    if ($user->hasRole(['tenant'])) {

      return redirect(url('/console'));

    } else {

      return redirect(url('/admin'));

    }
  }

  public function index()
  {
    return view('dashboard.tenant', $this->vdata);
  }

  public function select()
  {
    $user = Auth()->user();

    $tenants = $user->tenants;

    $this->vdata(compact('tenants'));

    return view('dashboard.select', $this->vdata);
  }

}
