<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StorePermission;
use App\Http\Requests\UpdatePermission;
use App\Permission;

class PermissionController extends AdminController
{
  public function __construct()
  {
    parent::__construct();

    $active = 'acl';

    $this->vdata(compact('active'));
  }

  /**
   * Display list
   * @param  Request $request
   * @return Response
   */
  public function index(Request $request)
  {
    $this->page_title = trans('permission.list');

    $list = Permission::paginate();

    $this->new_path = url('admin/permissions/new');

    $this->new_desc = trans('permission.new');

    $this->new_path_attr = "v-modal";

    $this->vdata(compact('list'));

    return view('permission.list', $this->vdata);

  }

  /**
   * Display the create form
   * @return Response
   */
  public function create()
  {
    $this->page_title = trans('permission.new');

    $this->layout = 'layouts.modal';

    return view('permission.new', $this->vdata);
  }

  /**
   * Display the edit form
   * @param  int $id - Role id
   * @return Response
   */
  public function edit($id)
  {
    $permission = Permission::findOrFail($id);

    $this->page_title = trans('permission.edit');

    $this->layout = 'layouts.modal';

    $this->vdata(compact('permission'));

    return view('permission.edit', $this->vdata);
  }

  /**
   * Process storing
   * @param  Request $request
   * @return Response
   */
  public function store(StorePermission $request)
  {
    Permission::create($request->input());

    return $this->goodReponse();
  }

  /**
   * Process updating
   * @param  Request $request
   * @return Response
   */
  public function update(UpdatePermission $request)
  {
    $permission = Permission::findOrFail($request->id);

    $permission->update($request->input());

    return $this->goodReponse();
  }

}
