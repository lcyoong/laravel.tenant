<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Filters\TenantFilter;
use App\Tenant;
use App\Http\Requests\StoreTenant;
use App\Http\Requests\UpdateTenant;
use App\Events\TenantConnected;

class TenantController extends AdminController
{
  public function __construct()
  {
    parent::__construct();

    $active = 'tenant';

    $this->vdata(compact('active'));
  }

  /**
   * Display list
   * @return Response
   */
  public function index(Request $request)
  {
    $filter = $request->input();

    $filters = new TenantFilter($filter);

    $this->page_title = trans('tenant.list');

    $this->new_path = url('/admin/tenants/new');

    $this->new_desc = trans('tenant.new');

    $this->new_path_attr = "v-modal";

    $tenants = Tenant::filter($filters)->paginate();

    $this->vdata(compact('tenants', 'filter'));

    return view('tenant.list', $this->vdata);
  }

  /**
   * Display create form
   * @return Response
   */
  public function create()
  {
    $this->page_title = trans('tenant.new');

    $this->layout = 'layouts.modal';

    return view('tenant.new', $this->vdata);
  }

  /**
   * Display edit form
   * @param  int $id - Setting id
   * @return Response
   */
  public function edit($id)
  {
    $tenant = Tenant::findOrFail($id);

    $this->layout = 'layouts.modal';

    $this->page_title = trans('tenant.edit', ['name' => $tenant->ten_name]);

    $this->vdata(compact('tenant'));

    return view('tenant.edit', $this->vdata);
  }

  /**
   * Process storing
   * @param  Request $request
   * @return Response
   */
  public function store(StoreTenant $request)
  {
    Tenant::create($request->input());

    return $this->goodReponse();
  }

  /**
   * Process updating
   * @param  Request $request
   * @return Response
   */
  public function update(UpdateTenant $request)
  {
    $lead = Tenant::findOrFail($request->ten_id);

    $lead->update($request->input());

    return $this->goodReponse();
  }

  /**
   * Process connecting
   * @param  Request $request
   * @return Response
   */
  public function connect(Request $request, $id)
  {
    $tenant = Tenant::findOrFail($id);

    event(new TenantConnected($tenant));

    return $this->goodReponse();
  }

}
