<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Filters\SettingFilter;
use App\Setting;
use App\Http\Requests\StoreSetting;
use App\Http\Requests\UpdateSetting;

class SettingController extends AdminController
{
  public function __construct()
  {
    parent::__construct();

    $active = 'setting';

    $this->vdata(compact('active'));
  }

  /**
   * Display list
   * @return Response
   */
  public function index(Request $request)
  {
    $filter = $request->input();

    $filters = new SettingFilter($filter);

    $this->page_title = trans('setting.list');

    $this->new_path = url('/admin/settings/new');

    $this->new_desc = trans('setting.new');

    $this->new_path_attr = "v-modal";

    $settings = Setting::filter($filters)->paginate();

    $this->vdata(compact('settings', 'filter'));

    return view('setting.list', $this->vdata);
  }

  /**
   * Display create form
   * @return Response
   */
  public function create()
  {
    $this->page_title = trans('setting.new');

    $this->layout = 'layouts.modal';

    return view('setting.new', $this->vdata);
  }

  /**
   * Display edit form
   * @param  int $id - Setting id
   * @return Response
   */
  public function edit($id)
  {
    $setting = Setting::findOrFail($id);

    $this->page_title = trans('setting.edit', ['name' => $setting->set_code]);

    $this->layout = 'layouts.modal';

    $this->vdata(compact('setting'));

    return view('setting.edit', $this->vdata);
  }

  /**
   * Process storing
   * @param  Request $request
   * @return Response
   */
  public function store(StoreSetting $request)
  {
    Setting::create($request->input());

    return $this->goodReponse();
  }

  /**
   * Process updating
   * @param  Request $request
   * @return Response
   */
  public function update(UpdateSetting $request)
  {
    $lead = Setting::findOrFail($request->set_id);

    $lead->update($request->input());

    return $this->goodReponse();
  }

}
