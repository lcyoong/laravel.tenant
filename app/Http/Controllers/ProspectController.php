<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Prospect;

class ProspectController extends ConsoleController
{
  public function index()
  {
    $prospects = Prospect::paginate();

    $this->vdata(compact('prospects'));

    return view('console.prospect.list', $this->vdata);
  }
}
