<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AttachTenantUser;
use App\Tenant;
use App\User;

class TenantUserController extends AdminController
{
  /**
   * Display users form
   * @param  int $id - Setting id
   * @return Response
   */
  public function index($id)
  {
    $tenant = Tenant::with('users')->findOrFail($id);

    $this->layout = 'layouts.modal';

    $this->page_title = trans('tenant.users') . ' - ' . $tenant->ten_name;

    $users = User::toDropDown('id', 'name');

    $this->vdata(compact('tenant', 'users'));

    return view('tenant.user.list', $this->vdata);
  }

  /**
   * Process attachment
   * @param  Request $request
   * @return Response
   */
  public function attach(AttachTenantUser $request, $id)
  {
    $tenant = Tenant::findOrFail($id);

    $tenant->users()->attach($request->tenu_user);

    return $this->goodReponse();
  }

  /**
   * Process detachment
   * @param  Request $request
   * @return Response
   */
  public function detach(Request $request, $ten_id, $user_id)
  {
    $tenant = Tenant::findOrFail($ten_id);

    $tenant->users()->detach($user_id);

    return $this->goodReponse();
  }

}
