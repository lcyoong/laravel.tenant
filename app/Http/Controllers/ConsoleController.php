<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Code;

class ConsoleController extends BaseController
{
  public function __construct()
  {
    parent::__construct();

    $layout = 'layouts.console.master';

    $bin_status = Code::where('cod_group', '=', 'bin_status')->toDropDown('cod_key', 'cod_description');

    $this->vdata(compact('layout', 'bin_status'));
  }
}
