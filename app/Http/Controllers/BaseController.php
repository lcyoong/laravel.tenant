<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Setting;

class BaseController extends Controller
{
  protected $vdata = [];
  protected $auth = false;
  private $layout;
  private $page_title;
  private $new_path, $new_desc, $new_path_attr;
  private $active = '';

  public function __construct()
  {
    $layout = 'layouts.app';

    $active = '';

    $settings = (new Setting)->toDropDown('set_code', 'set_value');

    $this->vdata = compact('layout', 'active', 'settings');
  }

  /**
   * Merge view data of child class with those from base class
   * @param  array $data
   * @return void
   */
  public function vdata($data)
  {
    $this->vdata = $data + $this->vdata;
  }

  /**
   * Set a property as view data
   * @param string $property
   * @param string $value
   */
  public function __set($property, $value)
	{
		if (property_exists($this, $property)) {
			$this->vdata[$property] = $value;
		}
	}

  /**
   * Format a default good response
   * @param  string $message
   * @param  array $data
   * @return Response
   */
  public function goodReponse($message = '', $data = null, $cookie = null)
  {
    $response = response([
      'success' => true,
      'data' => $data,
      'message' => $message ? $message : trans('message.process_successful')
    ]);

    if (!is_null($cookie)) {
      return $response->cookie($cookie);
    }

    return $response;
  }
}
