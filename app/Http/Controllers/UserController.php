<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use App\Role;
use App\Http\Requests\StoreUser;
use App\Http\Requests\UpdateUser;
use Filters\UserFilter;
// use Repositories\UserRepository;
// use Repositories\RoleRepository;


class UserController extends AdminController
{
  public function __construct()
  {
    parent::__construct();

    $active = 'acl';

    $this->vdata(compact('active'));
  }

  /**
   * Display list
   * @param  Request $request
   * @return Response
   */
  public function index(Request $request)
  {
    $filter = $request->input();

    $filters = new UserFilter($filter);

    $this->page_title = trans('user.list');

    $list = User::filter($filters)->paginate();

    $this->new_path = url('admin/users/new');

    $this->new_desc = trans('user.new');

    $this->new_path_attr = "v-modal";

    $this->vdata(compact('list', 'filter'));

    return view('user.list', $this->vdata);
  }

  /**
   * Display the create form
   * @return Response
   */
  public function create()
  {
    $this->page_title = trans('user.new');

    $roles = Role::orderBy('name', 'asc')->toDropDown('id', 'display_name');

    $this->layout = 'layouts.modal';

    $this->vdata(compact('roles'));

    return view('user.new', $this->vdata);
  }

  /**
   * Display edit form
   * @param  int $id - User id
   * @return Response
   */
  public function edit($id)
  {
    $user = User::findOrFail($id);

    $roles = Role::orderBy('name', 'asc')->toDropDown('id', 'display_name');

    $this->page_title = trans('user.edit', ['name' => $user->name]);

    $this->layout = 'layouts.modal';

    $this->vdata(compact('user', 'roles'));

    return view('user.edit', $this->vdata);
  }

  /**
   * Process storing
   * @param  Request $request
   * @return Response
   */
  public function store(StoreUser $request)
  {
    $input = $request->input();

    $input['password'] = bcrypt('111111');

    $user = User::create($input);

    $user->roles()->sync([array_get($input, 'role')]);

    return $this->goodReponse();
  }

  /**
   * Process updating
   * @param  Request $request
   * @return Response
   */
  public function update(UpdateUser $request)
  {
    $input = $request->input();

    $user = User::findOrFail($request->id);

    $user->update($input);

    $user->roles()->sync([array_get($input, 'role')]);

    return $this->goodReponse();
  }

}
