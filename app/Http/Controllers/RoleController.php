<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreRole;
use App\Http\Requests\UpdateRole;
use App\Role;
use App\Permission;
use Auth;

class RoleController extends AdminController
{
  public function __construct()
  {
    parent::__construct();

    $active = 'acl';

    $this->vdata(compact('active'));
  }

  /**
   * Display list
   * @param  Request $request
   * @return Response
   */
  public function index(Request $request)
  {
    $this->page_title = trans('role.list');

    $list = Role::paginate();

    $this->new_path = url('admin/roles/new');

    $this->new_desc = trans('role.new');

    $this->new_path_attr = "v-modal";

    $this->vdata(compact('list'));

    return view('role.list', $this->vdata);

  }

  /**
   * Display the create form
   * @return Response
   */
  public function create()
  {
    $this->page_title = trans('role.new');

    $this->layout = 'layouts.modal';

    return view('role.new', $this->vdata);
  }

  /**
   * Display the edit form
   * @param  int $id - Role id
   * @return Response
   */
  public function edit($id)
  {
    $role = Role::findOrFail($id);

    $this->page_title = trans('role.edit', ['name' => $role->display_name]);

    $this->layout = 'layouts.modal';

    $this->vdata(compact('role'));

    return view('role.edit', $this->vdata);
  }

  /**
   * Process storing of role
   * @param  Request $request
   * @return Response
   */
  public function store(StoreRole $request)
  {
    Role::create($request->input());

    return $this->goodReponse();
  }

  /**
   * Process updating
   * @param  Request $request
   * @return Response
   */
  public function update(UpdateRole $request)
  {
    $role = Role::findOrFail($request->id);

    $role->update($request->input());

    return $this->goodReponse();
  }

  /**
   * Display permission list form
   * @param  Request $request
   * @return Response
   */
  public function permission(Request $request, $id)
  {
    Role::findOrFail($id);

    // $list = Permission::leftJoin('permission_role', function ($join) use($id) {
    //   $join->on('permission_id', '=', 'id')->where('role_id', '=', $id);
    // })->get();

    $this->page_title = trans('role.permission');

    $this->layout = 'layouts.modal';

    $this->vdata(compact('id'));

    return view('role.permission', $this->vdata);
  }

  /**
   * Attach new permission to role
   * @param Request $request
   * @param integer  $role
   * @param integer  $permission
   */
  // public function addPermission(Request $request, $id, $permission)
  // {
  //   $role = Role::findOrFail($id);
  //
  //   $role->permissions()->attach($permission);
  //
  //   return $this->goodReponse();
  // }

  /**
   * Sync permission to role
   * @param Request $request
   * @param integer  $role
   * @param integer  $permission
   */
  public function syncPermission(Request $request, $role)
  {
    $input = $request->input();

    $permission = array_get($input, 'toggled', []);

    // $this->repo->findById($role)->permissions()->sync(array_keys($permission));
    Role::findOrFail($role)->perms()->sync(array_keys($permission));

    return $this->goodReponse();
  }
}
