<?php

namespace App\Http\Middleware;

use Closure;
use App\Tenant;

class AssignedTenant
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      $ten_id = $request->route('tenant');

      if ($request->user()->tenants()->where('ten_id', '=', $ten_id)->count() > 0 || $request->user()->hasRole('super_admin')) {

        if (!session()->has('tenant') || $ten_id != session('tenant')->acc_id) {

          $tenant = Tenant::findOrFail($ten_id);

          session(['tenant' => $tenant]);

        }

        return $next($request);

      } else {

        return abort(403);

      }
    }
}
