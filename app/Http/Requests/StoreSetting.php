<?php

namespace App\Http\Requests;

class StoreSetting extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'set_code' => 'required|unique:settings,set_code',
        'set_value' => 'required|max:255',
      ];
    }
}
