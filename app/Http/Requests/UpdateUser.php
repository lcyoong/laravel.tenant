<?php

namespace App\Http\Requests;

class UpdateUser extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'name' => 'required',
        'email' => 'required|unique_but_self:users,email,id',
        'role' => 'required',
      ];
    }
}
