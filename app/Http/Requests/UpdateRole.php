<?php

namespace App\Http\Requests;

class UpdateRole extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'name' => 'required|unique_but_self:roles,name,id',
          'display_name' => 'required|max:255',
        ];
    }
}
