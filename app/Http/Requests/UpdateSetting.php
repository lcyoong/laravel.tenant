<?php

namespace App\Http\Requests;

class UpdateSetting extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'set_code' => 'required|unique_but_self:settings,set_code,set_id',
        'set_value' => 'required|max:255',
      ];
    }
}
