<?php

namespace App\Http\Requests;

class UpdatePermission extends BaseRequest
{
  public function rules()
  {
    return [
      'name' => 'required|unique_but_self:permissions,name,id',
      'display_name' => 'required|max:255',
    ];
  }
}
