<?php

namespace App\Http\Requests;

class StoreTenant extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'ten_name' => 'required|max:255',
        ];
    }
}
