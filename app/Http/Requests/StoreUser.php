<?php

namespace App\Http\Requests;

class StoreUser extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
      return [
        'name' => 'required',
        'email' => 'required|unique:users,email',
        'role' => 'required',
      ];
    }

}
