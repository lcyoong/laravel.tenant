<?php

namespace App\Http\Requests;

class StorePermission extends BaseRequest
{
  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'name' => 'required|unique:permissions,name',
      'display_name' => 'required|max:255',
    ];
  }
}
