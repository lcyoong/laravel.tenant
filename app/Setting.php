<?php

namespace App;

class Setting extends BaseModel
{
  protected $audit = true;

  protected $primaryKey = 'set_id';

  protected $fillable = ['set_', 'set_code', 'set_value'];
}
