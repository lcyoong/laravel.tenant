<?php

namespace App;

class AuditTrail extends BaseModel
{
  protected $primaryKey = 'au_id';

  protected $fillable = ['au_model_type', 'au_mode', 'au_model_id', 'au_data', 'created_by'];

  public function au_model()
  {
    return $this->morphTo();
  }
}
