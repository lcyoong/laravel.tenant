<?php

namespace App\Traits;

use App\AuditTrail;

trait AuditTrailRelationship
{
  public function auditTrails()
  {
    return $this->morphMany('App\AuditTrail', 'au_model')->orderBy('au_id', 'desc');
  }

}
