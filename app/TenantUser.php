<?php

namespace App;

class TenantUser extends BaseModel
{
  protected $table = 'tenant_users';

  public function user()
  {
    return $this->belongsTo(TenantUser::class, 'tenu_user');
  }
}
