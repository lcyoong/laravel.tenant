<?php

namespace App\Listeners;

use App\Events\ModelCreated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\AuditTrail;

class AuditLogCreated
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ModelCreated  $event
     * @return void
     */
    public function handle(ModelCreated $event)
    {
      if ($event->post['audit']) {

        $auditTrail = new AuditTrail([
          'au_mode' => 'created',
          'au_data' => serialize(array_diff($event->post['attributes'], $event->post['original']))
        ]);

        $event->post->auditTrails()->save($auditTrail);
      }
    }
}
