<?php

namespace App\Listeners;

use App\Events\TenantConnected;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Wgroup\Tenant;
use DB;
use Artisan;
use Log;

class CreateAndMigrateDatabase
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AccountConnected  $event
     * @return void
     */
    public function handle(TenantConnected $event)
    {
      $tenant = $event->tenant;

      $database = "owlsight_" . $tenant->ten_db;

      Log::info($database);

      DB::statement("CREATE DATABASE $database");

      Tenant::connect(['database' => $database]);

      Artisan::call('migrate', array('--database' => $database, '--path' => '/database/migrations/tenant'));

    }
}
