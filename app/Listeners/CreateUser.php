<?php

namespace App\Listeners;

use App\Events\UserNeeded;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class CreateUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ModelCreated  $event
     * @return void
     */
    public function handle(UserNeeded $event)
    {
      if ($event->post['user']) {

        $input['name'] = $event->post->{$event->post['user']['name']};

        $input['email'] = $event->post->{$event->post['user']['email']};

        if (isset($event->post['user']['password'])) {
          $input['password'] = $event->post->{$event->post['user']['password']};
        } else {
          $input['password'] = bcrypt('111111');
        }

        $user = User::create($input);

        $user->roles()->sync([$event->post['user']['role']]);

        $event->post->user()->associate($user);

        $event->post->save();
      }
    }
}
