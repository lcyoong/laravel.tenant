<?php

return [
  'list' => 'Users List',
  'new' => 'New User',
  'edit' => 'Edit User - :name',
  'id' => 'ID',
  'name' => 'Name',
  'email' => 'Email',
  'role' => 'Role',
  'password' => 'Password',
  'password_confirmation' => 'Confirm Password',
  'change_password' => 'Change password',
];
