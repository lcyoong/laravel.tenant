<?php

return [
  'list' => 'Settings List',
  'new' => 'New Setting',
  'edit' => 'Edit Setting - :name',
  'set_code' => 'Code',
  'set_value' => 'Value',
];
