<?php

return [
  'list' => 'Tenants List',
  'new' => 'New Tenant',
  'shadow' => 'Shadow',
  'edit' => 'Edit Tenant - :name',
  'ten_id' => 'ID',
  'ten_name' => 'Tenant name',
  'ten_expiry' => 'Expiry',
  'ten_db' => 'Database',
  'ten_status' => 'Status',
  'tenu_user' => 'User',
  'users' => 'Users',
];
