<?php

return [
  'hello' => 'Hello, :name',
  'logout' => 'Logout',
  'change_password' => 'Change Password',
  'dashboard' => 'Dashboard',
  'settings' => 'Settings',
  'access' => 'Access',
  'users' => 'Users',
  'roles' => 'Roles',
  'permissions' => 'Permissions',
  'tenants' => 'Tenants',
  'prospects' => 'Prospects',
  'settings' => 'Settings',
];
