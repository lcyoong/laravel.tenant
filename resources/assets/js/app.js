
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('./main');

window.util = require('./utilities');
window.mixForm = require('./mixins/form.js');
window.mixResponse = require('./mixins/response.js');

require('./directives/modal.js');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('form-ajax', require('./components/AjaxForm.vue'));
Vue.component('post-ajax', require('./components/AjaxPost.vue'));
Vue.component('vue-select', require('./components/Select2.vue'));
Vue.component('bootstrap-toggler', require('vue-bootstrap-toggle'));

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
);

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': Laravel.csrfToken
    }
});
