module.exports = {

  isJson: function (str) {
      try {
          JSON.parse(str);
      } catch (e) {
        console.log(e)
          return false;
      }
      return true;
  },

  onCompleteNotify: function (response) {

    // $(el).prop('disabled', false);

    if (response.data) {
      var data = JSON.parse(response.data)
      toastr.success(data.message)
      // $.notify(response.data.message, {
      //   position: "bottom right",
      //   className: "success"
      // });
    } else {
      toastr.info("Completed without message.")
      // $.notify("Completed without message.", {
      //   position: "bottom right",
      //   className: "info"
      // });
    }

  },

  onErrorNotify: function (response) {

    var data = JSON.parse(response.data)

    if (util.isJson(data.message)) {
      var message = JSON.parse(data.message)
      // var json = $.parseJSON(message);

      $.each(message, function(key, value) {

        toastr.error(value)

        // $.notify(value, {
        //   position: "bottom right",
        //   className: "error"
        // });
      });
    } else {
      var message = data.message
      toastr.error(message)
      // $.notify(response.data.message, {
      //   position: "bottom right",
      //   className: "error"
      // });
    }
  },

  showModal: function (path) {

    $('#basicModal').find('.modal-content').html('');
    $('#basicModal').modal('show');
    $('#basicModal').find('.modal-content').load(path);

  }


};
