module.exports = {
  data: {
    waiting: false,
  },

  methods: {

    startWait: function () {
      this.waiting = true
      $("#ajax_loader").show();
    },

    endWait: function () {
      this.waiting = false
      $("#ajax_loader").hide();
    }

  }
}
