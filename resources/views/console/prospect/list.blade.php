@extends($layout)

@section('content_above_list')
@endsection

@section('content_list')
<table class="table table-condensed table-striped table-hover">
  <thead>
    <tr>
      <th>@lang('setting.set_code')</th>
      <th>@lang('setting.set_value')</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($prospects as $prospect)
      <tr>
        <td>{{ $prospect->pro_full_name }}</td>
        <td>{{ $prospect->pro_status }}</td>
      </tr>
    @endforeach
  </tbody>
</table>
@endsection

@prepend('content')
<div id="prospect-list">
@include('layouts.console.list', ['count' => $prospects->total(), 'links' => $prospects])
</div>
@endprepend

@push('scripts')
<script>

new Vue({
  el: '#prospect-list',
});

</script>
@endpush
