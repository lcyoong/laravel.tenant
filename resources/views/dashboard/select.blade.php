@extends($layout)

@section('content_page')
@endsection

@prepend('content')
<div id="console-select">
@include('layouts.console.page')
</div>
@endprepend

@prepend('scripts')
<script>
new Vue ({
  el: "#console-select",
  mixins: [mixForm],
})
</script>
@endprepend
