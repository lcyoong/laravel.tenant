@extends($layout)

@section('content_page')
@endsection

@prepend('content')
<div id="console-dashboard">
@include('layouts.console.page')
</div>
@endprepend

@prepend('scripts')
<script>
new Vue ({
  el: "#console-dashboard",
  mixins: [mixForm],
})
</script>
@endprepend
