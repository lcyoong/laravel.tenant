@extends($layout)

@section('portlet_password_change')
  <form-ajax action = "{{ url('change_password') }}" method="POST" @startwait="startWait" @endwait="endWait" reload-on-complete=1>
    {{ Form::hidden('id', auth()->user()->id) }}
    <div class="row">
      {{ Form::bsPassword('password', trans('user.password')) }}
      {{ Form::bsPassword('password_confirmation', trans('user.password_confirmation')) }}
    </div>
    {{ Form::submit(trans('form.save'), ['class' => 'btn btn-primary btn-sm btn-circle red', ':disabled' => 'waiting']) }}
  </form-ajax>
@endsection

@section('content_page')
<div id="password-change">
@include('layouts.portlet', ['portlet_body' => 'portlet_password_change'])
</div>
@endsection

@prepend('content')
@include('layouts.admin.page')
@endprepend

@prepend('scripts')
<script>
new Vue ({

  el: "#password-change",

  mixins: [mixForm],
})
</script>
@endprepend
