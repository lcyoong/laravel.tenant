<!-- MAIN NAVIGATION -->
<div id="menu" class="menu right-float clearfix">
  <a href="#" class="responsive-nav-toggle"><span class="hamburger"></span></a>
    <div class="menu-inner">

      <nav id="main-nav">
        <ul>
          <li @activeMenu($active, 'services')><a href="{{ url('/services') }}">Services</a></li>
          <li @activeMenu($active, 'about')><a href="{{ url('/about-wgroup') }}">About WGroup</a></li>
          <!-- <li @activeMenu($active, 'resources')><a href="{{ url('/resources') }}">Resource Center</a></li> -->
          <li @activeMenu($active, 'insights')><a href="{{ url('/insights') }}">Insights</a></li>
          <li @activeMenu($active, 'careers')><a href="{{ url('/careers') }}">Careers</a></li>
          <li @activeMenu($active, 'contact')><a href="{{ url('/contact-us') }}">Contact Us</a></li>
        </ul>
      </nav>

      <!-- <div id="menu-misc" class="clearfix">
        <div id="header-search">
          <a href="#" id="show-search"><i class="fa fa-search"></i></a>
          <div class="header-search-content">
            <form action="{{ url('/search') }}" method="get">
              <a href="#" id="close-search"></a>
              <input type="text" name="q" class="form-control" value="" placeholder="Enter your search ...">
              <h5 class="subtitle-1">... & press enter to start</h5>
            </form>
           <div class="search-outer"></div>
          </div>
        </div>
      </div> -->
      <!-- END #menu-misc -->

    </div><!-- END .menu-inner -->
</div>
