<!-- Header -->
<header id="header" class="header-bordered header-transparent transparent-light sub-dark">
  <div class="header-inner clearfix">

      <!-- LOGO -->
      <div id="logo" class="left-float">
        <a href="{{ url('/') }}">
          <img id="scroll-logo" src="{{ asset('img/logo-scroll.png') }}" alt="Logo on Scroll">
          <img id="dark-logo" src="{{ asset('img/logo-dark.png') }}" alt="Logo Dark">
          <img id="light-logo" src="{{ asset('img/logo-light.png') }}" alt="Logo Light">
        </a>
      </div>

      @include('partials.menu')
   </div>
</header>
