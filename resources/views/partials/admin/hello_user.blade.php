<li class="dropdown">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">@lang('nav.hello', ['name' => auth()->user()->name])<span class="caret"></span></a>
  <ul class="dropdown-menu">
    <li><a href="{{ url('/admin/change_password') }}">@lang('nav.change_password')</a></li>
    <li><a href="{{ url('/logout') }}">@lang('nav.logout')</a></li>
  </ul>
</li>
