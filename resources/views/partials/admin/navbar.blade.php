<div class="hor-menu">
<ul class="nav navbar-nav">
  <li class="menu-dropdown classic-menu-dropdown @activeAdminMenu($active, "'dashboard'")"><a href="{{ url('admin') }}">@lang('nav.dashboard')</a></li>
  @permitted('manage_tenant')
  <li class="@activeAdminMenu($active, "'tenant'")"><a href="{{ url('/admin/tenants') }}">@lang('nav.tenants')</a></li>
  @endpermitted

  @permitted(['manage_user', 'manage_role', 'manage_permission'])
  <li class="menu-dropdown classic-menu-dropdown @activeAdminMenu($active, "'acl'")">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">@lang('nav.access') <span class="caret"></span></a>
    <ul class="dropdown-menu pull-left">
      @permitted('manage_user')
      <li><a href="{{ url('/admin/users') }}">@lang('nav.users')</a></li>
      @endpermitted
      @permitted('manage_role')
      <li><a href="{{ url('/admin/roles') }}">@lang('nav.roles')</a></li>
      @endpermitted
      @permitted('manage_permission')
      <li><a href="{{ url('/admin/permissions') }}">@lang('nav.permissions')</a></li>
      @endpermitted
    </ul>
  </li>
  @endpermitted

  @permitted('manage_settings')
  <li class="@activeAdminMenu($active, "'setting'")"><a href="{{ url('/admin/settings') }}">@lang('nav.settings')</a></li>
  @endpermitted

</ul>
</div>
