<div class="page-wrapper-row">
  <div class="page-wrapper-bottom">
      <!-- BEGIN FOOTER -->
      <!-- BEGIN INNER FOOTER -->
      <div class="page-footer">
          <div class="container-fluid"> {{ date('Y') }} © {{ config('app.name') }}. All rights reserved.</div>
      </div>
      <div class="scroll-to-top" style="display: block;">
          <i class="icon-arrow-up"></i>
      </div>
      <!-- END INNER FOOTER -->
      <!-- END FOOTER -->
  </div>
</div>
