<div class="page-logo">
  <a href="{{ url(env('APP_URL') . '/console') }}">
    <img src="http://portal.thinkwgroup.com/img/wgroup-logo.png" alt="logo" class="logo-default">
  </a>
</div>
<a href="javascript:;" class="menu-toggler"></a>
<div class="top-menu">
  <ul class="nav navbar-nav pull-right">
    @include('partials.console.selected')
    @include('partials.console.hello_user')
  </ul>
</div>
