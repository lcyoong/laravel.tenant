@if(session()->has('tenant'))
<li class="dropdown">
  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> {{ session('tenant')->ten_name }}<span class="caret"></span></a>
</li>
@endif
