<div class="hor-menu">
<ul class="nav navbar-nav">
  <li class="menu-dropdown classic-menu-dropdown @activeAdminMenu($active, "'dashboard'")"><a href="{{ url("/console") }}">@lang('nav.dashboard')</a></li>
  @permitted('manage_prospect')
  <li class="@activeAdminMenu($active, "'prospect'")"><a href="@url(prospects)">@lang('nav.prospects')</a></li>
  @endpermitted
</ul>
</div>
