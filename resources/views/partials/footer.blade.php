<!-- Footer -->
<footer id="footer" class="footer-dark text-light">
    <div class="column-section clearfix adapt-height">
      <div class="column one-half pull-half">
        <div class="copyright"><small>{{ date('Y') }} © {{ config('app.name') }}. All rights reserved.</small></div>
      </div>
      <div class="column one-half push-half last-col">
        <div id="menu_footer">
          <nav id="footer-nav">
            <ul>
              <!-- <li class="current-menu-item"><a href="#">Sitemap</a></li> -->
              <li><a href="{{ url('/terms-conditions') }}">Terms &amp; Conditions</a></li>
              <li><a href="{{ url('/privacy-policy') }}">Privacy Policy</a></li>
            </ul>
          </nav>

          <div id="footer-menu-misc">
            <div class="social-link">
              <ul class="socialmedia-widget hover-fade-1">
                <li class="googleplus"><a href="https://plus.google.com/103945615627061822969" target=_blank></a></li>
                <li class="linkedin"><a href="https://www.linkedin.com/company-beta/2449839/" target=_blank></a></li>
                <li class="twitter"><a href="https://twitter.com/thinkWGroup" target=_blank></a></li>
              </ul>
            </div>
          </div><!-- END #menu-misc -->
        </div>
      </div>
    </div>
</footer>
