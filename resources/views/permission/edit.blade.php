@extends($layout)

@prepend('content')
<div id="permission-edit">
<form-ajax action = "{{ url('admin/permissions/update') }}" method="POST" redirect-on-complete="{{ url('admin/permissions') }}" @startwait="startWait" @endwait="endWait">
  {{ Form::hidden('id', $permission->id) }}
  <div class="row">
    {{ Form::bsText('name', trans('role.name'), $permission->name) }}
    {{ Form::bsText('display_name', trans('role.display_name'), $permission->display_name) }}
  </div>
  {{ Form::submit(trans('form.save'), ['class' => 'btn btn-circle red btn-sm', ':disabled' => 'waiting']) }}
</form-ajax>
</div>
@endprepend

@prepend('scripts')
<script>
new Vue ({
  el: "#permission-edit",
  mixins: [mixForm],
})

</script>
@endprepend
