@extends($layout)

@prepend('content')
<div id="permission-new">
<form-ajax action = "{{ url('admin/permissions/new') }}" method="POST" redirect-on-complete="{{ url('admin/permissions') }}" @startwait="startWait" @endwait="endWait">
  <div class="row">
    {{ Form::bsText('name', trans('permission.name')) }}
    {{ Form::bsText('display_name', trans('permission.display_name')) }}
  </div>
  {{ Form::submit(trans('form.save'), ['class' => 'btn btn-circle red btn-sm', ':disabled' => 'waiting']) }}
</form-ajax>
</div>
@endprepend

@prepend('scripts')
<script>
new Vue ({
  el: "#permission-new",
  mixins: [mixForm],
})

</script>
@endprepend
