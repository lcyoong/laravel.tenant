<div class="col-md-{{ $col }}">
  <div class="form-group">
      {!! Form::labelWithHTML($name, $label, ['class' => 'control-label']) !!}
      <div>
        <select class="addselect2"
          @foreach($attributes as $key => $value)
          {{ $key }} = "{{ $value }}"
          @endforeach
        >
          <option disabled value="0">Select one</option>
        </select>
      </div>
  </div>
</div>
