<div class="col-md-{{ $col }}">
  <div class="form-group">
      {{ Form::label($name, $label, ['class' => 'control-label']) }}
      <div>
        <script type="text/x-template" id="demo-template">
          <div>
            {{ $attributes['options'] }}
            <select2 :options="{{ $attributes['options'] }}" v-model="{{ $attributes['v-model'] }}">
              <option disabled value="0">Select one</option>
            </select2>
          </div>
        </script>

        <script type="text/x-template" id="select2-template">
          <select>
            <slot></slot>
          </select>
        </script>
      </div>
  </div>
</div>
