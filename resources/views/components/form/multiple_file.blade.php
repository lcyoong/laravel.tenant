<span class="btn green fileinput-button">
  <i class="fa fa-plus"></i>
  <span> {{$label}} </span>
  <input type="file" name="{{$name}}[]" multiple=""
  @foreach(array_merge(['class' => 'form-control'], $attributes?: []) as $attrkey => $attrvalue)
  {{ $attrkey }} = "{{ $attrvalue }}"
  @endforeach
  >
</span>
