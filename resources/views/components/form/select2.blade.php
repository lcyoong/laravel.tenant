<div class="col-md-{{ $col }}">
  <div class="form-group">
      {!! Form::labelWithHTML($name, $label, ['class' => 'control-label']) !!}
      <div>
      <vue-select name="{{ $name }}"
        @foreach($attributes as $key => $value)
        {{ $key }} = "{{ $value }}"
        @endforeach
      >
      <option disabled value="0">Select one</option>
      </vue-select>
      </div>
  </div>
</div>
