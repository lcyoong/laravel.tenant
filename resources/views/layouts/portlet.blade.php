<div class="portlet light portlet-fit">
  @if(isset($title))
  <div class="portlet-title"><div class="caption"><span class="caption-subject font-blue bold">{{ $title }}</span></div>
  <div class="actions">@yield(isset($portlet_actions) ? $portlet_actions : '')</div>
  </div>
  @endif
  <div class="portlet-body">
    @yield($portlet_body)
  </div>
</div>
