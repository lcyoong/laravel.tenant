<div class="page-content">
  <div class="container-fluid">
    <div class="page-content-inner">
      @yield('content_page')
    </div>
  </div>
</div>
