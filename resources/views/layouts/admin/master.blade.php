<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('meta')
    <title>{{ $page_title or config('myapp.title') }}</title>
    <link href="{{ asset(mix('css/app.css')) }}" rel="stylesheet">
    <link href="{{ asset('build/toastr/toastr.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/global/css/components.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/global/css/plugins.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/pages/css/profile.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/layouts/layout3/css/layout.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/layouts/layout3/css/themes/default.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/layouts/layout3/css/custom.css') }}" rel="stylesheet">
    @yield('head_content')
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body class="page-container-bg-solid">
  <div id="app" class="page-wrapper">
    <div class="page-wrapper-row">
      <div class="page-wrapper-top">
        <div class="page-header">
          <div class="page-header-top">
            <div class="container-fluid">
              @include('partials.admin.topbar')
            </div>
          </div>
          <div class="page-header-menu">
            <div class="container-fluid">
              @include('partials.admin.navbar')
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="page-wrapper-row full-height">
      <div class="page-wrapper-middle">
        <div class="page-container">
          <div class="page-content-wrapper">
            <div class="page-head">
              <div class="container-fluid">
                <div class="page-title">
                  <h1>{{ $page_title or '' }}</h1>
                </div>
                <div class="page-toolbar" id="toolbar">
                  @if(isset($new_path))
                      <div class="btn-group btn-theme-panel">
                        <a href="{{ $new_path }}" class="btn dropdown-toggle" {{ $new_path_attr or ''}}><i class="icon-plus" style="vertical-align:middle"></i> {{ $new_desc or 'New' }}</a>
                      </div>
                  @endif
                </div>
              </div>
            </div>
            @stack('content')
          </div>
        </div>
      </div>
    </div>
    @include('partials.admin.footer')
  </div>
  <script src="{{ asset(mix('js/app.js')) }}"></script>
  <script src="{{ asset('build/toastr/toastr.min.js') }}"></script>
  <!-- <script src="{{ asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script> -->
  <script src="{{ asset('assets/global/scripts/app.js') }}"></script>
  <script src="{{ asset('assets/layouts/layout3/scripts/layout.js') }}"></script>
  <script>
  Vue.config.productionTip = false
  Vue.config.devtools = false
  new Vue({
    el: '#toolbar',
  });
  </script>
  @stack('scripts')
  @include('partials.admin.modal')
  @include('partials.loader')
</body>
</html>
