@section('portlet_body_list')
<span>{{ $count or 0 }} @lang('form.records')</span>
<div class="row">
  <div class="col-md-{{ $left_section_col or 12 }}">
  <!-- <div class="table-responsive"> -->
  <table class="table table-bordered table-hover table-striped table-condensed flip-content">
  @yield('content_list')
  </table>
  <!-- </div> -->
  </div>
</div>
@if(isset($links)){{ $links->appends(Request::input())->links() }}@endif
@endsection

@if(!isset($modal))
<div class="page-content">
  <div class="container-fluid">
@endif
    <div class="page-content-inner">
      @yield('content_above_list')
      @include('layouts.portlet', ['portlet_body' => 'portlet_body_list'])
    </div>
@if(!isset($modal))
  </div>
</div>
@endif
