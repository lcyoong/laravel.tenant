@extends($layout)

@prepend('content')
<div id="user-edit">
<form-ajax action = "{{ url('admin/users/update') }}" method="POST" redirect-on-complete="{{ url('admin/users') }}" @startwait="startWait" @endwait="endWait">
  {{ Form::hidden('id', $user->id) }}
  <div class="row">
    {{ Form::bsText('name', trans('user.name'), $user->name) }}
    {{ Form::bsText('email', trans('user.email'), $user->email) }}
    {{ Form::bsSelect('role', trans('user.role'), $roles, $user->roles->first()->id) }}
  </div>
  {{ Form::submit(trans('form.save'), ['class' => 'btn btn-primary btn-sm btn-circle red', ':disabled' => 'waiting']) }}
</form-ajax>
</div>
@endprepend

@prepend('scripts')
<script>
new Vue ({
  el: "#user-edit",
  mixins: [mixForm],
})

</script>
@endprepend
