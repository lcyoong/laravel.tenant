@extends($layout)

@prepend('content')
<div id="user-new">
  <form-ajax action = "{{ url('admin/users/new') }}" method="POST" redirect-on-completex="{{ url('admin/users') }}" @startwait="startWait" @endwait="endWait">
    <div class="row">
      {{ Form::bsText('name', trans('user.name')) }}
      {{ Form::bsText('email', trans('user.email')) }}
      {{ Form::bsSelect('role', trans('user.role'), $roles) }}
    </div>
    {{ Form::submit(trans('form.save'), ['class' => 'btn btn-primary btn-sm btn-circle red', ':disabled' => 'waiting']) }}
  </form-ajax>
</div>
@endprepend

@prepend('scripts')
<script>
new Vue ({
  el: "#user-new",
  mixins: [mixForm],
})
</script>
@endprepend
