@extends($layout)

@section('content_above_list')
@include('tenant.filter')
@endsection

@section('content_list')
  <table class="table table-condensed table-striped table-hover">
    <thead>
      <tr>
        <th>@lang('tenant.ten_id')</th>
        <th>@lang('tenant.ten_name')</th>
        <th>@lang('tenant.ten_db')</th>
        <th>@lang('tenant.ten_status')</th>
        <th>@lang('form.actions')</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($tenants as $tenant)
        <tr>
          <td>{{ $tenant->ten_id }}</td>
          <td>{{ $tenant->ten_name }}</td>
          <td>{{ $tenant->ten_db }}</td>
          <td>{{ $tenant->ten_status }}</td>
          <td>
            <a v-modal href="{{ url(sprintf('admin/tenants/%s/edit', $tenant->ten_id)) }}" title="@lang('form.edit')"><i class="fa fa-edit"></i></a>
            <a v-modal href="{{ url(sprintf('admin/tenants/%s/users', $tenant->ten_id)) }}" title="@lang('tenant.users')"><i class="fa fa-users"></i></a>
            <a href="{{ url(sprintf('console/%s', $tenant->ten_id)) }}" title="@lang('tenant.shadow')" target=_blank><i class="fa fa-user-secret"></i></a>
            <post-ajax post-to="{{ url("admin/tenants/{$tenant->ten_id}/connect") }}"><i class="fa fa-database"></i></post-ajax>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
@endsection

@prepend('content')
<div id="tenant-list">
@include('layouts.admin.list', ['count' => $tenants->total(), 'links' => $tenants])
</div>
@endprepend

@push('scripts')
<script>

new Vue({
  el: '#tenant-list',
});

</script>
@endpush
