@section('portlet_body_filter')
{{ Form::open(['url' => 'admin/tenants', 'method' => 'get']) }}
<div class="row">
  {{ Form::filterText('name', trans('tenant.ten_name'), array_get($filter, 'name'), ['placeholder' => trans('tenant.ten_name')]) }}
</div>
{{ Form::submit(trans('form.filter'), ['class' => 'btn btn-primary btn-sm btn-circle red']) }}
{!! Form::linkButton(url('admin/tenants'), trans('form.clear')) !!}
{{ Form::close() }}
@endsection

@include('layouts.portlet', ['portlet_body' => 'portlet_body_filter'])
