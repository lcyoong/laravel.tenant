@extends($layout)

@prepend('content')
<div id="tenant-edit">
  <form-ajax action = "{{ url('admin/tenants/update') }}" method="POST" reload-on-complete="1" @startwait="startWait" @endwait="endWait">
    {{ Form::hidden('ten_id', $tenant->ten_id) }}
    <div class="row">
      {{ Form::bsText('ten_name', trans('tenant.ten_name'), $tenant->ten_name) }}
      {{ Form::bsText('ten_db', trans('tenant.ten_db'), $tenant->ten_db) }}
      {{ Form::bsSelect('ten_status', trans('tenant.ten_status'), $bin_status, $tenant->ten_status) }}
    </div>
    {{ Form::submit(trans('form.save'), ['class' => 'btn btn-primary btn-sm btn-circle red', ':disabled' => 'waiting']) }}
  </form-ajax>
</div>
@endprepend

@prepend('scripts')
<script>
new Vue ({
  el: "#tenant-edit",
  mixins: [mixForm],
})

</script>
@endprepend
