@extends($layout)

@prepend('content')
<div id="tenant-users">
  <form-ajax action = "{{ url("/admin/tenants/{$tenant->ten_id}/users/new") }}" method="POST" @startwait="startWait" @endwait="endWait">
    <div class="row">
      {{ Form::bsSelect('tenu_user', trans('tenant.tenu_user'), $users) }}
    </div>
    {{ Form::submit(trans('form.save'), ['class' => 'btn btn-primary btn-sm btn-circle red', ':disabled' => 'waiting']) }}
  </form-ajax>
  @foreach($tenant->users as $user)
  <post-ajax post-to="{{ url("admin/tenants/{$tenant->ten_id}/users/{$user->id}/remove") }}"><i class="fa fa-times"></i></post-ajax> {{ $user->name }}
  @endforeach
</div>
@endprepend

@prepend('scripts')
<script>
new Vue ({
  el: "#tenant-users",
  mixins: [mixForm],
})

</script>
@endprepend
