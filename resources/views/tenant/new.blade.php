@extends($layout)

@prepend('content')
<div id="tenant-new">
  <form-ajax action = "{{ url('admin/tenants/new') }}" method="POST" reload-on-complete=1 @startwait="startWait" @endwait="endWait">
    <div class="row">
      {{ Form::bsText('ten_name', trans('tenant.ten_name')) }}
      {{ Form::bsText('ten_db', trans('tenant.ten_db')) }}
    </div>
    {{ Form::submit(trans('form.save'), ['class' => 'btn btn-primary btn-sm btn-circle red', ':disabled' => 'waiting']) }}
  </form-ajax>
</div>
@endprepend

@prepend('scripts')
<script>
new Vue ({
  el: "#tenant-new",
  mixins: [mixForm],
})
</script>
@endprepend
