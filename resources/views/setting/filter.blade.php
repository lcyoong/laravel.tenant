@section('portlet_body_filter')
{{ Form::open(['url' => '/admin/settings', 'method' => 'get']) }}
<div class="row">
  {{ Form::filterText('code', trans('setting.set_code'), array_get($filter, 'code'), ['placeholder' => trans('setting.set_code')]) }}
</div>
{{ Form::submit(trans('form.filter'), ['class' => 'btn btn-primary btn-sm btn-circle red']) }}
{!! Form::linkButton(url('/admin/settings'), trans('form.clear')) !!}
{{ Form::close() }}
@endsection

@include('layouts.portlet', ['portlet_body' => 'portlet_body_filter'])
