@extends($layout)

@prepend('content')
<div id="setting-edit">
<form-ajax action = "{{ url('/admin/settings/update') }}" method="POST" redirect-on-complete="{{ url('/admin/settings') }}" @startwait="startWait" @endwait="endWait">
  {{ Form::hidden('set_id', $setting->set_id) }}
  <div class="row">
    {{ Form::bsText('set_code', trans('setting.set_code'), $setting->set_code) }}
    {{ Form::bsText('set_value', trans('setting.set_value'), $setting->set_value) }}
  </div>
  {{ Form::submit(trans('form.save'), ['class' => 'btn btn-primary btn-sm btn-circle red', ':disabled' => 'waiting']) }}
</form-ajax>
</div>
@endprepend

@prepend('scripts')
<script>
new Vue ({
  el: "#setting-edit",
  mixins: [mixForm],
})

</script>
@endprepend
