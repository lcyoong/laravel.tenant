@extends($layout)

@prepend('content')
<div id="setting-new">
  <form-ajax action = "{{ url('/admin/settings/new') }}" method="POST" reload-on-complete="1" @startwait="startWait" @endwait="endWait">
    <div class="row">
      {{ Form::bsText('set_code', trans('setting.set_code')) }}
      {{ Form::bsText('set_value', trans('setting.set_value')) }}
    </div>
    {{ Form::submit(trans('form.save'), ['class' => 'btn btn-primary btn-sm btn-circle red', ':disabled' => 'waiting']) }}
  </form-ajax>
</div>
@endprepend

@prepend('scripts')
<script>
new Vue ({
  el: "#setting-new",
  mixins: [mixForm],
})
</script>
@endprepend
