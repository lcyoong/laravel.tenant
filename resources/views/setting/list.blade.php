@extends($layout)

@section('content_above_list')
@include('setting.filter')
@endsection

@section('content_list')
  <table class="table table-condensed table-striped table-hover">
    <thead>
      <tr>
        <th>@lang('setting.set_code')</th>
        <th>@lang('setting.set_value')</th>
        <th>@lang('form.actions')</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($settings as $setting)
        <tr>
          <td>{{ $setting->set_code }}</td>
          <td>{{ $setting->set_value }}</td>
          <td>
            <a v-modal href="{{ url(sprintf('admin/settings/%s/edit', $setting->set_id)) }}" title="@lang('form.edit')"><i class="fa fa-edit"></i></a>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
@endsection

@prepend('content')
<div id="setting-list">
@include('layouts.admin.list', ['count' => $settings->total(), 'links' => $settings])
</div>
@endprepend

@push('scripts')
<script>

new Vue({
  el: '#setting-list',
});

</script>
@endpush
