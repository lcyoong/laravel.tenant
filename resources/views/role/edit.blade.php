@extends($layout)

@prepend('content')
<div id="role-edit">
<form-ajax action = "{{ url('admin/roles/update') }}" method="POST" redirect-on-complete="{{ url('admin/roles') }}" @startwait="startWait" @endwait="endWait">
  {{ Form::hidden('id', $role->id) }}
  <div class="row">
    {{ Form::bsText('name', trans('role.name'), $role->name) }}
    {{ Form::bsText('display_name', trans('role.display_name'), $role->display_name) }}
  </div>
  {{ Form::submit(trans('form.save'), ['class' => 'btn btn-circle red btn-sm', ':disabled' => 'waiting']) }}
  <redirect-btn label="@lang('form.cancel')" redirect="{{ url('roles') }}" class="btn-sm"></redirect-btn>
</form-ajax>
</div>
@endprepend

@prepend('scripts')
<script>
new Vue ({
  el: "#role-edit",
  mixins: [mixForm],
})

</script>
@endprepend
