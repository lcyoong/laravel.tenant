@extends($layout)

@prepend('content')
<div id="role-new">
<form-ajax action = "{{ url('admin/roles/new') }}" method="POST" redirect-on-complete="{{ url('admin/roles') }}" @startwait="startWait" @endwait="endWait">
  <div class="row">
    {{ Form::bsText('name', trans('role.name')) }}
    {{ Form::bsText('display_name', trans('role.display_name')) }}
  </div>
  {{ Form::submit(trans('form.save'), ['class' => 'btn btn-circle red btn-sm', ':disabled' => 'waiting']) }}
  <redirect-btn label="@lang('form.cancel')" redirect="{{ url('roles') }}" class="btn-sm"></redirect-btn>
</form-ajax>
</div>
@endprepend

@prepend('scripts')
<script>
new Vue ({

  el: "#role-new",

  mixins: [mixForm],

  data: {
  },

  methods: {
  }

})

</script>
@endprepend
