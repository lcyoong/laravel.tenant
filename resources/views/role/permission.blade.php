@extends($layout)

@section('content_list')
<table class="table table-condensed table-striped table-hover">
  <thead>
    <tr>
      <th>@lang('permission.id')</th>
      <th>@lang('permission.name')</th>
      <th>@lang('permission.display_name')</th>
      <th>@lang('form.actions')</th>
    </tr>
  </thead>
  <tbody>
    <tr v-for="permission in permissions">
      <td>@{{ permission.id }}</td>
      <td>@{{ permission.name }}</td>
      <td>@{{ permission.display_name }}</td>
      <td>
        <bootstrap-toggler :name="'toggled['+permission.id+']'" @input="toggled(permission)" v-model="permission.active" data-size="normal"/>
      </td>
    </tr>
  </tbody>
</table>
@endsection

@prepend('content')
<div id="permission-list">
  <form-ajax action = "{{ url("admin/roles/$id/permissions") }}" method="POST" @startwait="startWait" @endwait="endWait">
  @include('layouts.admin.list', ['modal' => true])
  {{ Form::submit(trans('form.save'), ['class' => 'btn btn-circle red btn-sm', ':disabled' => 'waiting']) }}
  </form-ajax>
</div>
@endprepend

@push('scripts')
<script>

new Vue({
  el: '#permission-list',

  mixins: [mixForm, mixResponse],

  data: {
    permissions: [],
    role_perms: [],
  },

  created: function () {
    this.getRolePermissions()
  },

  methods: {
    toggled: function(permission) {
      console.log(permission)
    },

    getRolePermissions: function () {
      this.$http.get('{{ url("api/v1/role/$id/permissions") }}')
          .then(function (response) {
            console.log(response.data)
            this.permissions = response.data
          });
    },

    // addPermission: function (id) {
    //   this.$http.post('{{ url("roles/$id/permissions/add") }}/' + id)
    //       .then(function (response) {
    //         console.log(response.data)
    //       });
    // },
  }

});

</script>
@endpush
