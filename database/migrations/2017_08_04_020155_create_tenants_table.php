<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTenantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenants', function (Blueprint $table) {
            $table->increments('ten_id');
            $table->string('ten_name');
            $table->string('ten_db');
            $table->date('ten_expiry')->nullable();
            $table->string('ten_status')->default('active');
            $table->integer('created_by');
            $table->timestamps();
        });

        Schema::create('tenant_users', function (Blueprint $table) {
            $table->increments('tenu_id');
            $table->integer('tenu_tenant');
            $table->integer('tenu_user')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('tenant_users');

      Schema::dropIfExists('tenants');
    }
}
