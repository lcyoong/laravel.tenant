<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('codes', function (Blueprint $table) {
          $table->increments('cod_id');
          $table->string('cod_group');
          $table->string('cod_key');
          $table->string('cod_description');
          $table->integer('cod_order')->default(0);
          $table->string('cod_status')->default('active');
          $table->integer('created_by');
          $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('codes');
    }
}
