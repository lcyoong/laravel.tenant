<?php

use Illuminate\Database\Seeder;

class AuthSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        ['id'=>1, 'name' => 'Admin','email' => 'wg.services@thinkwgroup.com', 'password' => bcrypt('111111')],
      ]);

      DB::table('roles')->insert([
        ['id'=>1, 'name' => 'super_admin', 'display_name' => 'Super Admin'],
        ['id'=>2, 'name' => 'wgops', 'display_name' => 'WG Operation'],
        ['id'=>3, 'name' => 'account', 'display_name' => 'Account'],
      ]);

      DB::table('permissions')->insert([
        ['id'=>1, 'name' => 'manage_user', 'display_name' => 'Manage Users'],
        ['id'=>2, 'name' => 'manage_role', 'display_name' => 'Manage Roles'],
        ['id'=>3, 'name' => 'manage_permission', 'display_name' => 'Manage Permissions'],
        ['id'=>4, 'name' => 'system_setting', 'display_name' => 'System setting'],
      ]);

      DB::table('role_user')->insert([
        ['user_id' => 1, 'role_id'=>1],
      ]);

      DB::table('permission_role')->insert([
      ]);
    }
}
