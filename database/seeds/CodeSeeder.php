<?php

use Illuminate\Database\Seeder;

class CodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('codes')->insert([
        ['cod_group' => 'bin_status','cod_key' => 'active', 'cod_description' => 'Active', 'created_by' => 1],
        ['cod_group' => 'bin_status','cod_key' => 'suspended', 'cod_description' => 'Suspended', 'created_by' => 1],
      ]);
    }
}
